CREATE TABLE boeken(
Voornaam varchar(50) char set utf8mb4,
Familienaam varchar(80) char set utf8mb4,
Titel  varchar(255) char set utf8mb4,
Stad varchar(50) char set utf8mb4,
Verschijningsjaar varchar(4),
Uitgeverij varchar(80) char set utf8mb4,
Herdruk varchar(4),
Commentaar varchar(1000)
);